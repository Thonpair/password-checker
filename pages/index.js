import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { useState } from 'react';
import PasswordAnalyser from '../components/PasswordAnalyser';

export default function Home() {
  const [password,setPassword] = useState("");
  return (
    <div className={styles.container}>
      <Head>
        <title>Know your password</title>
        <meta name="description" content="Check the strength of your password and get some recommendations" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Know your password
        </h1>

        <p className={styles.description}>
         Check the strength of your password
        </p>

        <div className={styles.form}>
          <input 
            type="text" 
            className={styles.inputtext} 
            value={password} 
            onChange={(e)=>setPassword(e.target.value)} 
            placeholder="Write your password here" />
          <PasswordAnalyser password={password} />
        </div>
   
      </main>

      <footer className={styles.footer}>
       
      </footer>
    </div>
  )
}
