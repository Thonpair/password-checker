import zxcvbn from "../vendors/zxcvbn"
import styles from '../styles/PasswordAnalyser.module.css'
import { useState, useEffect } from "react";

export default function PasswordAnalyser({ password }) {
    const [score, setScore] = useState(0)
    const [time, setTime] = useState("")
    const [feedback, setFeedback] = useState({})

    useEffect(() => {
        const passwordAnalyse = zxcvbn(password);
        setScore(passwordAnalyse.score);
        setTime(passwordAnalyse.crack_times_display.offline_slow_hashing_1e4_per_second);
        setFeedback(passwordAnalyse.feedback);
        console.log(passwordAnalyse.feedback)
    }, [password])
    return (
        <>
            <meter
                className={styles.progressbar}
                value={score}
                low="2" high="3" optimum="4"
                min="0" max="4" >
            </meter>
            <p>Score : {score}</p>
            {password.length > 0 &&
                    <p>guess time : {time}</p>
            }
            {feedback.warning !== undefined &&
                feedback.warning.length > 0 &&
                <p>warning : {feedback.warning}</p>
            }
            {feedback.suggestions !== undefined &&
                feedback.suggestions.map(s=> 
                <p key={s.id}>{s}</p>)
            }
        </>
    )
}